﻿//Soal1();
//Soal2();
//Soal3();
//Soal4();
//Soal5();
//Soal6();
//soal7();
//Soal8();
//Soal9();
Soal10();
//Coba();

Console.ReadKey();

static void Coba()
{
    Console.WriteLine(" === Soal 4 === ");
    Console.Write(" Masukkan Tanggal Mulai (dd/MM/yyyy) : ");
    int[] arrTglMulai = Array.ConvertAll( Console.ReadLine().Split("/"), int.Parse);
    DateTime tglMulai = new DateTime(arrTglMulai[2], arrTglMulai[1], arrTglMulai[0]);

    Console.Write(" Masukkan Lama Kelas (hari) : ");
    int lamaKelas = int.Parse(Console.ReadLine());

    Console.Write("Masukkan TGL Libur (koma) : ");
    int[] arrtglLibur = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

    DateTime tglSelesai = tglMulai.AddDays(-1);

    for (int i = 0; i < lamaKelas ; i++)
    { 
        //cek hari libur
        for (int j = 0; j <arrtglLibur.Length ; j++)
        {
            if (tglSelesai.Day == arrtglLibur[j]) 
            {
                tglSelesai = tglSelesai.AddDays(1);
                //cek sabtu dan minggu
                if (tglSelesai.DayOfWeek == DayOfWeek.Saturday)
                {
                    tglSelesai = tglSelesai.AddDays(2);
                }
            }
        }
        tglSelesai = tglSelesai.AddDays(1);

        //cek sabtu dan minggu
        if (tglSelesai.DayOfWeek == DayOfWeek.Saturday)
        {
            tglSelesai = tglSelesai.AddDays(2);
        }
    }

    DateTime tglUjianFT1 = tglSelesai.AddDays(1);
    if (tglSelesai.DayOfWeek == DayOfWeek.Saturday)
    {
        tglUjianFT1 = tglUjianFT1.AddDays(2);
    }
    Console.WriteLine($" Batch#319 akan ujian FT1 pada : {tglUjianFT1.ToString("dd/MM/yyyy")}");
}

static void Soal10()
{
    string lilin;
    int total = 0;

    Console.Write("Tinggi Lilin = ");
    lilin = Console.ReadLine();
    string[] Lilin = lilin.Split(' ');
    int[] ConvertLilin = Array.ConvertAll(Lilin, int.Parse);

    int tinggililin = ConvertLilin.Max();
    for (int i = 0; i < ConvertLilin.Length; i++)
    {
        if (tinggililin == ConvertLilin[i])
        {
            total++;
        }
    }
    Console.Write($"Jumlah Lilin Yang Di Tiup {total}");

}
static void Soal9()
{
    /*Console.Write("Masukkan jumlah baris: ");
    int rows = int.Parse(Console.ReadLine());

    Console.Write("Masukkan jumlah kolom: ");
    int cols = int.Parse(Console.ReadLine());

    int[,] data = new int[rows, cols];

    Console.WriteLine("Masukkan elemen matriks:");

    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            Console.Write($"Masukkan elemen data[{i}, {j}]: ");
            data[i, j] = int.Parse(Console.ReadLine());
        }
    }

    int temp = 0;
    int temp1 = 0;

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            if (i == j)
            {
                temp += data[i, j];
            }
            if (i == i - 0 - 0 && j == 3 - i - 1)
            {
                temp1 += data[i, j];
            }
        }
    }
    Console.WriteLine($"Perbedaan Diagonal = {temp - temp1}");
}*/

    int[,] data = new int[,]
    {
                {11,2,4},
                {4,5,6 },
                {10,8,-12}
    };
    int temp = 0;
    int temp1 = 0;
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            if (i == j)
            {
                temp += data[i, j];
            }
            if (i == i - 0 - 0 && j == 3 - i - 1)
            {
                temp1 += data[i, j];
            }
        }
    }
    Console.WriteLine($"Perbedaan Diagonal = {temp - temp1}");
}

static void Soal8()
{
    Console.WriteLine(" === Soal 8 === ");
    Console.Write("Masukkan nilai N: ");
    int N = int.Parse(Console.ReadLine());
    for (int i = 0; i <= N; i++)
    {
        for (int j = 0; j <= N; j++)
        {
            if (j <= N - i)
            {
                Console.Write(" ");
            }
            else
            {
                Console.Write("#");
            }

        }
        Console.Write("\n");
    }
    //// Menampilkan pola 
    //for (int i = 1; i <= N; i++)
    //{
    //    for (int j = N; j >= i; j--)
    //    {
    //        Console.Write(" ");
    //    }
    //    for (int k = 1; k <= i ; k++)
    //    {
    //        Console.Write("#");
    //    }

    //    Console.WriteLine();
    //}
}


static void soal7()
{
    int menu;
    int alergi;
    string harga_menu;
    int uang_elsa;

    Console.Write("Total Menu = ");
    menu = int.Parse(Console.ReadLine());
    Console.Write("Index Makanan Alergi = ");
    alergi = int.Parse(Console.ReadLine());
    Console.Write("Harga Menu = ");
    harga_menu = Console.ReadLine();
    string[] hargamenu = harga_menu.Split(',');
    Console.Write("Jumlah Uang Elsa = ");
    uang_elsa = int.Parse(Console.ReadLine());

    int[] harga = Array.ConvertAll(hargamenu, int.Parse);

    int sum = 0;
    for (int i = 0; i < harga.Length; i++)
    {
        sum += harga[i];
    }
    int jumlah = sum - harga[alergi];
    int total = jumlah / 2;
    int sisauang = uang_elsa - total;

    Console.WriteLine();
    Console.WriteLine($"Elsa Harus Membayar =Rp. {total}");

    if (sisauang > 0)
    {
        Console.WriteLine($"Sisa Uang = Rp. {sisauang}");
    }
    else if (sisauang < 0)
    {
        Console.WriteLine($"Uang Elsa kurang = Rp. {sisauang}");
    }
    else
    {
        Console.WriteLine("Uang Elsa Pas");
    }
}

static void Soal6()
{
    Console.WriteLine(" === Soal 6 ===");
    Console.Write("Input : ");
    string input = Console.ReadLine().ToLower().Replace(" ","");

    // Menampilkan setiap karakter dalam input di baris baru dengan tanda '*' sebelum dan sesudahnya
    foreach (char c in input)
    {
        Console.WriteLine($"***{c}***");
    }
}

static void Soal5()
{
    string kalimat;
    Console.Write("Masukkan Kalimat = ");
    kalimat = Console.ReadLine();

    int countVokal = 0;
    int countKonsonan = 0;

    //string vokal = "aiueo";

    for (int i = 0; i < kalimat.Length; i++)
    {
        char huruf = char.ToLower(kalimat[i]);
        if(huruf >= 'a' && huruf <= 'z')
        {

        
        if(huruf ==  'a'  || huruf == 'i'|| huruf == 'u' || huruf == 'e' || huruf == 'o')
        {
            countVokal++;
        }
        /*if (vokal.Contains(huruf))
        {
            countVokal++;
        }*/
        /*else if (char.IsLetter(huruf))
        {
            countKonsonan++;
        }*/
        else
        {
            countKonsonan++;
        }
      }
    }

    Console.WriteLine($"Huruf Vokal = {countVokal}");
    Console.WriteLine($"Huruf Konsonan = {countKonsonan}");
}

   // string kata;
   // Console.Write("Masukkan Kata = ");
   // kata = Console.ReadLine();

   // List<string> Vocal = new List<string>();
   // List<string> Konsonan = new List<string>();

    

   // for (int i = 0; i < kata.Length; i++)
   // {
   //     char huruf = kata.ToLower()[i];

   //     if (huruf.ToString() == "a" || huruf.ToString() == "i" || huruf.ToString() == "u" || huruf.ToString() == "e" || huruf.ToString() == "i")
   //     {
   //         Vocal.Add(huruf.ToString());
//   //     }
//   //     else
//   //     {
//   //         Konsonan.Add(huruf.ToString());
//   //     }
//   // }
//   ///* int countVokal = 0;
//   // int countKonsonan = 0;*/
//   // Console.WriteLine($"Jumlah Huruf Vokal = {Vocal}");
//   // Console.WriteLine($"Jumlah Huruf Konsonan = {Konsonan}");
//   // /* Vocal.Sort();
//     Konsonan.Sort();
//     Konsonan.Reverse();
//     Console.Write("Huruf Vokal = "
//         );
//     foreach (string kalimat in Vocal)
//     {
//         Console.Write(kalimat);
//     }
//     Console.WriteLine();
//     Console.Write("Huruf Konsonan = ");
//     foreach (string kalimat1 in Konsonan)
//     {
//         Console.Write(kalimat1);
//     }*


//}



static void Soal4()
{
    Console.Write("Masukkan Tanggal Masuk : ");
    string tanggalMasuk = Console.ReadLine();
    DateTime mulai = DateTime.Parse(tanggalMasuk);

    Console.Write("Masukan Tanggal Libur : ");
    string[] libur = Console.ReadLine().Split(',');

    for (int i = 1; i <= 11; i++)
    {
        int date = mulai.Day;
        int hari = (int)mulai.DayOfWeek;
        if (hari == 6 || hari == 0)
        {
            i--;
        }
        else if (Array.IndexOf(libur, date.ToString()) != -1)
        {
            i--;
        }
        else
            Console.WriteLine($"Tanggal {date} hari ke {i}.");
        if (i == 11)
        {
            break;
        }
        mulai = mulai.AddDays(1);
    }
    Console.WriteLine($"Kelas akan ujian pada {mulai}");
} 

static void Soal3()
{

    Console.Write("Masukkan Tanggal Peminjaman Buku = ");
    string tanggal = Console.ReadLine();
    DateTime meminjam = DateTime.Parse(tanggal);

    Console.Write("Masukkan Tanggal Pengembalian Buku = ");
    string pengembalian = Console.ReadLine();
    DateTime balikin = DateTime.Parse(pengembalian);

    int selisihHari = (balikin - meminjam).Days;
    int denda = 500;
    int totalDenda = 0;
    
    if (selisihHari > 3)
    {
        int jarak = selisihHari - 3 ;
        totalDenda = jarak * denda;
        Console.WriteLine($" Mono harus membayar denda sebesar : {totalDenda}");
    }
    else
    {
        Console.Write("Terimakasih Telah Mengembalikan Buku Tepat Waktu");
    }
    /*int totalDenda = 0;

    if (balikin >= meminjam)
    {
        TimeSpan jarak = balikin - meminjam.AddDays(3);
        int denda = (int)jarak.TotalDays;
        totalDenda = denda * 500;
        Console.WriteLine($" Mono harus membayar denda sebesar : {totalDenda}");
    }
    else
    {
        Console.Write("Terimakasih Telah Mengembalikan Buku Tepat Waktu");
    }*/
}


static void Soal2()
{
    Console.WriteLine(" === Soal 2 === ");
    try
    {
        Console.Write("Masukkan pesan Morse: ");
        string morseMessage = Console.ReadLine();

        int errorCount = CountErrors(morseMessage);
        Console.WriteLine("Total Sinyal salah: " + errorCount);

     
        static int CountErrors(string morseMessage)
        {
            string sosPattern = "SOS";
            int errorCount = 0;

            for (int i = 0; i < morseMessage.Length; i += 3)
            {
                string subString = morseMessage.Substring(i, 3);
                if (subString != sosPattern)
                {
                    errorCount++;
                }
            }

            return errorCount;
        }
    }

    catch (Exception)
    {
        Console.WriteLine(" HARUS SOS !!! ");
    }
}

    

static void Soal1()
{
    try
    {
        int faktorial = 1;
        int total = 1;

        Console.Write("Masukkan X = ");

        for (int i = 1; i <= faktorial; i++)
        {
            total *= i;
        }
        Console.Write($"Ada {total} Cara");
    }
    catch (Exception)
    {
        Console.WriteLine(" Nilai X hanya berupa Angka !!! ");
    }

}