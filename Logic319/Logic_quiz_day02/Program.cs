﻿//Soal1();
//Soal2();
//Soal3();
//Soal4();
//Soal5();
//Soal6();


Console.ReadKey();

static void Soal6()
{
    Console.WriteLine("--Soal 6--");
    Console.Write("Masukan Nilai Angka : ");
    int angka = int.Parse(Console.ReadLine());

    if (angka / 2 == 0)
    {
        Console.WriteLine("Angka " + angka + " adalah Genap");
    }
    else
    {
        Console.WriteLine("Angka " + angka + " adalah Ganjil");
    }
}

static void Soal5()
{
    Console.WriteLine("--Soal 5--");
    Console.Write("Masukan Nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    string grade;

    if (nilai >= 80)
    {
        Console.WriteLine(" Grade Nilai A");
    }
    else if (nilai >= 60)
    {
        Console.Write(" Grade Nilai B");
    }
    else
    {
        Console.Write(" Grade Nilai C");
    }


}

static void Soal4()
{
    Console.WriteLine("--Soal 4--");
    Console.Write(" Masukan jumlah puntung rokok yang didapatkan :");
    int puntungRokok = int.Parse(Console.ReadLine());

    int batangRokok = puntungRokok / 8;
    Console.WriteLine("Jumlah batang rokok yang berhasil dirangkai:" + batangRokok);


    int sisaPuntung = puntungRokok % 8;
    Console.WriteLine(" Sisa puntung rokok:" + sisaPuntung);

    int HargaPerBatang = 500;
    int Hasil = batangRokok * HargaPerBatang;

    Console.WriteLine("Penghasilan Pemulung: RP " + Hasil);
}

static void Soal3()
{
    Console.WriteLine("--Soal 3--");
    Console.Write("Masukan angka A: ");
    int AngkaA = int.Parse(Console.ReadLine());
    Console.Write("Masukan angka B: ");
    int AngkaB = int.Parse(Console.ReadLine());

    if (AngkaA % AngkaB == 0)
    {
        Console.WriteLine("Hasil Modulus = 0");
        Console.WriteLine($"{AngkaA} % {AngkaB} = 0");
    }
    else
    {
        int HasilModulus = AngkaA % AngkaB;
        Console.WriteLine("Hasil Modulus = {Hasil Modulus}");
        Console.WriteLine($"{AngkaA} % {AngkaB} = {HasilModulus}");
    }

}

static void Soal2()
{
    Console.WriteLine("--Soal 2--");
    Console.Write("Masukan Nilai S : ");
    int S = int.Parse(Console.ReadLine());

    int LuasPersegi;
    int KelilingPersegi;

    LuasPersegi = S * S;
    Console.WriteLine($"Luas Persegi = {LuasPersegi}");

    KelilingPersegi = 4 * S;
    Console.WriteLine($"Keliling Persegi = {KelilingPersegi}");
}

static void Soal1()
{
    Console.WriteLine("--Soal 1--");
    Console.Write("Masukan Nilai Jari-Jari :");
    double jari = Convert.ToDouble(Console.ReadLine());



    double LuasLingkaran;
    double KelilingLingkaran;

    LuasLingkaran = Math.PI * jari * jari;
    Console.WriteLine($"LuasLingkaran = {Math.Round(LuasLingkaran)}");

    KelilingLingkaran = 2 * Math.PI * jari;
    Console.WriteLine($"KelilingLingkungan = {Math.Round(KelilingLingkaran)}");
}