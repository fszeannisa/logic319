﻿//Soal1();
//Soal2();
//Soal3();
//Soal4();
//Soal5();
//Soal6();
//Soal7();
Soal8();


Console.ReadKey();


static void Soal8()
{
    Console.WriteLine("--Soal 8--");
    Console.Write("Masukkan Nilai MATEMATIKA : ");
    double matematika = Convert.ToDouble(Console.ReadLine());
    Console.Write("Masukkan Nilai Fisika : ");
    double fisika = Convert.ToDouble(Console.ReadLine());
    Console.Write("Masukkan Nilai Kimia : ");
    double kimia = Convert.ToDouble(Console.ReadLine());

    double ratarata = (matematika + fisika + kimia) / 3;

    Console.WriteLine("Nilai Rata-rata : " + ratarata);

    if (ratarata >= 50 && ratarata <= 100)
    {
        Console.WriteLine("Selamat\n Kamu Berhasil\n Kamu Hebat");

    }
    else if (ratarata >= 0 && ratarata < 50)
    {
        Console.WriteLine("Maaf \n Kamu Gagal");
    }
    else
    {
        Console.WriteLine(" Nilai harus 0 sampai 100");
    }
}

static void Soal7()
{
    Console.WriteLine("--Soal 7--");
    Console.WriteLine();
    Console.Write("Masukkan berat badan anda\t: ");
    float kilogram = float.Parse(Console.ReadLine());
    Console.Write("Masukkan tinggi badan anda\t: ");
    float centimeter = float.Parse(Console.ReadLine());
    float meter = centimeter / 100;
    float meterkuadrat = meter * meter;
    float bmi = kilogram / meterkuadrat;
    Console.WriteLine($"Nilai BMI anda adalah\t: {bmi}");
    if (bmi < 18.5)
    {
        Console.Write("Anda kurus");
    }
    else if (bmi >= (185 / 10) && bmi < 25)
    {
        Console.Write("Anda Langsing");
    }
    else if (bmi > 25)
    {
        Console.Write("Anda Gemuk");
    }
    else
    {
        Console.WriteLine("Masukan data yang benar");
    }
}
static void Soal6()
{
    Console.Write("Nama: ");
    string nama = Console.ReadLine();

    Console.Write("Tunjangan: ");
    int tunjangan = Convert.ToInt32(Console.ReadLine());

    Console.Write("Gapok: ");
    int gapok = Convert.ToInt32(Console.ReadLine());

    Console.Write("Banyak Bulan: ");
    int banyakBulan = Convert.ToInt32(Console.ReadLine());

    double pajak, bpjs, gajiPerBulan, totalGaji;

    double totalPendapatan = gapok + tunjangan;

    if (totalPendapatan <= 5000000)
    {
        pajak = 0.05 * totalPendapatan;
    }
    else if (totalPendapatan <= 10000000)
    {
        pajak = 0.1 * totalPendapatan;
    }
    else
    {
        pajak = 0.15 * totalPendapatan;
    }

    bpjs = 0.03 * totalPendapatan;

    gajiPerBulan = totalPendapatan - (pajak + bpjs);
    totalGaji = gajiPerBulan * banyakBulan;

    Console.WriteLine("\nKaryawan atas nama {0} slip gaji sebagai berikut:", nama);
    Console.WriteLine("Pajak\t\tRp{0:#,0}", pajak);
    Console.WriteLine("BPJS\t\tRp{0:#,0}", bpjs);
    Console.WriteLine("Gaji/bulan\tRp{0:#,0}", gajiPerBulan);
    Console.WriteLine("Total gaji/{0} bulan\tRp{1:#,0}", banyakBulan, totalGaji);
}

static void Soal5()
{
    Console.WriteLine("--Soal 5--");
    atas:
    bool Ulangi = true;
    string name = "";

    while (Ulangi)
    {
        if (string.IsNullOrEmpty(name))
        {
            Console.Write("Masukkan nama anda : ");
            name = Console.ReadLine();

            if (string.IsNullOrEmpty(name))
            {
                Console.WriteLine("Nama tidak boleh kosong. Silakan coba lagi.");
                goto atas;
            }
        }

        int tahun;
        Console.Write("Tahun berapa anda lahir : ");
        string tahunInput = Console.ReadLine();

        if (!int.TryParse(tahunInput, out tahun))
        {
            Console.WriteLine("Tahun lahir tidak boleh kosong dan harus berupa angka. Silakan coba lagi.");
            continue;
        }

        if (tahun >= 1944 && tahun <= 1964)
        {
            Console.WriteLine($"{name}, berdasarkan tahun lahir anda tergolong 'Baby Boomer'");
        }
        else if (tahun >= 1965 && tahun <= 1979)
        {
            Console.WriteLine($"{name}, berdasarkan tahun lahir anda tergolong 'Generasi X'");
        }
        else if (tahun >= 1980 && tahun <= 1994)
        {
            Console.WriteLine($"{name}, berdasarkan tahun lahir anda tergolong 'Generasi Y(Millennials)'");
        }
        else if (tahun >= 1995 && tahun <= 2015)
        {
            Console.WriteLine($"{name}, berdasarkan tahun lahir anda tergolong 'Generasi Z'");
        }
        else if (tahun >= 2016 && tahun <= 2023)
        {
            Console.WriteLine($"{name}, berdasarkan tahun lahir anda tergolong 'Bau kencur'");
        }
        else if (tahun >= 2024)
        {
            Console.WriteLine($"{name}, berdasarkan tahun lahir anda tergolong 'Belum Lahir'");
        }
        else
        {
            Console.WriteLine($"{name}, berdasarkan tahun lahir anda tergolong 'Sudah Tua'");
        }

        Console.Write("Ulangi Proses (Y/N) : ");
        string input = Console.ReadLine();

        if (input.ToLower() == "n")
        {
            Ulangi = false;
        }
        else
        {
            name = string.Empty;
        }
    }
}

static void Soal4()
{
    int cost_shop, shipping, voucher;
    Console.WriteLine("-== MARKET PLACE ==-");

    try
    {
        Console.WriteLine("Input Your Cost Shop\n: ");
        cost_shop = int.Parse(Console.ReadLine());
        Console.WriteLine("Input your Shipping\n: ");
        shipping = int.Parse(Console.ReadLine());
        Console.WriteLine("Choose Your Voucher : (1/2/3) ");
        voucher = int.Parse(Console.ReadLine());

        int free_shipping = 0;
        int discount = 0;

        switch (voucher)
        {
            case 1:

                if (cost_shop >= 30000)
                {
                    free_shipping = 5000;
                    discount = 5000;

                    free_shipping = free_shipping > shipping ? shipping : free_shipping;


                }

                break;

            case 2:
                if (cost_shop >= 50000)
                {
                    free_shipping = 10000;
                    discount = 10000;

                    free_shipping = free_shipping > shipping ? shipping : free_shipping;
                }

                break;

            case 3:
                if (cost_shop >= 100000)
                {
                    free_shipping = 20000;
                    discount = 10000;

                    free_shipping = free_shipping > shipping ? shipping : free_shipping;
                }

                break;

            default:
                Console.WriteLine("Failed Input Voucher");
                break;

        }
        Console.WriteLine($"Shop : {cost_shop}");
        Console.WriteLine($"Shipping : {shipping}");
        Console.WriteLine($"Free Shipping: {free_shipping}");
        Console.WriteLine($"Discount : {discount}");
        Console.WriteLine($"Total Shopping : {cost_shop + shipping - free_shipping - discount}");
    }
    catch (Exception e)
    {

        Console.WriteLine("Must number and not empty!");
        Console.WriteLine(e.Message);
    }
}
static void Soal3()
{
    Console.WriteLine("--Soal 6--");
    double diskon = 0, belanja, ongkir, total = 0;

    Console.WriteLine("Program Diskon OVO Grab");
    Console.Write("Inputkan Total Belanja \t = ");
    int shop = int.Parse(Console.ReadLine());
    Console.Write("Inputkan Jarak \t = ");
    int jarak = int.Parse(Console.ReadLine());
    Console.Write("Kode Promo \t = ");
    string promo = Console.ReadLine();

    //ongkir = jarak * 1000;
    ongkir = jarak <= 5 ? 5000 : jarak * 1000;
    Console.WriteLine();
    if (promo.ToUpper() == "JKTOVO" && shop >= 30000)
    {
        diskon = shop * 0.4;
        if (diskon > 30000)
        {
            diskon = 30000;
        }
    }
    Console.WriteLine($"Belanja \t : {shop} ");
    Console.WriteLine($"Diskon 40% \t : {diskon} ");
    Console.WriteLine($"Ongkir \t \t : {ongkir} ");
    Console.WriteLine($"Total Belanja \t : {shop + ongkir - diskon} ");
}
static void Soal2()
{
    Console.WriteLine("-- Soal 2--");
    Console.Write(" Masukan Jumlah Pembelian Pulsa :");
    int jumlahPulsa = int.Parse(Console.ReadLine());
    int point = 0;

    if (jumlahPulsa > 0 && jumlahPulsa < 5000)
    {
        point = 0;
        Console.WriteLine(" Anda tidak memperoleh point : " + point);
    }
    else if (jumlahPulsa >= 10000 && jumlahPulsa < 25000)
    {
        point = 80;
        Console.WriteLine(" Jumlah point yang diperoleh : " + point);
    }

    else if (jumlahPulsa >= 25000 && jumlahPulsa < 50000)
    {
        point = 200;
        Console.WriteLine(" Jumlah point yang diperoleh : " + point);
    }
    else if (jumlahPulsa >= 50000 && jumlahPulsa == 100000)
    {
        point = 400;
        Console.WriteLine(" Jumlah point yang diperoleh : " + point);
    }
    else
    {
        Console.WriteLine(" Masukan Range Harga 10.000 - 100.000");
    }
}
static void Soal1()
{
    Console.WriteLine("---Soal 1---");
    Console.Write("Masukkan Nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    string grade;

    if (nilai >= 90 && nilai <= 100)
    {
        grade = "A";
        Console.WriteLine("Grade = " + grade);
    }
    else if (nilai >= 70 && nilai <= 89)
    {
        grade = "B";
        Console.WriteLine("Grade = " + grade);
    }
    else if (nilai >= 50 && nilai <= 69)
    {
        grade = "C";
        Console.WriteLine("Grade = " + grade);
    }
    else if (nilai >= 0 && nilai < 50)
    {
        grade = "E";
        Console.WriteLine("Grade = " + grade);
    }
    else
    {
        Console.WriteLine("Masukkan Nilai dari 0 sampai 100");
    }
}