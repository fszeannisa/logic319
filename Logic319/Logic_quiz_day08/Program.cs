﻿//Soal1();
//Soal2();
//Soal3();
//Soal4();
//Soal5();
Soal6();

Console.ReadKey();
static void Soal6()
{
    Console.WriteLine(" === Soal 6 === ");
    try
    {
        
        Console.Write("Masukkan nilai N \t: ");
        int N = int.Parse(Console.ReadLine());
        
        Console.Write("Masukkan elemen array \t : ");
        string[] arrStr = Console.ReadLine().Split(',');
        int[] arr = Array.ConvertAll(arrStr, int.Parse);

        if (arr.Length == N)
        {
            // Menggunakan algoritma Bubble Sort
            for (int i = 0; i < N - 1; i++)
            {
                for (int j = 0; j < N - i - 1; j++)
                {
                    if (arr[j] > arr[j + 1])
                    {
                        int temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }

            Console.WriteLine($" {string.Join(",", arr)}");
            /*Console.Write("Array terurut : \t");
            foreach (int x in arr)
            {
                Console.Write(x + " ");
            }*/
        }
        else
        {
            Console.WriteLine(" Jumlah Elemen Array Harus Sama dengan nilai N");
        }
    }
    catch (Exception)
    {
        Console.WriteLine(" Masukan Input N dan Elemen Array Hanya Berupa ANGKA AJA YA !!! ");
        
    }
   
}

    static void Soal5()
{
    Console.WriteLine(" === Soal 5 ===");
    Console.Write("Masukkan elemen array : ");
    string[] arrStr = Console.ReadLine().Split(',');
    int[] arr = Array.ConvertAll(arrStr, int.Parse);

    Console.Write("Masukkan jumlah rotasi: ");
    int rot = int.Parse(Console.ReadLine());

    Console.WriteLine("Output:");

    for (int i = 0; i < rot; i++)
    {
        RotateLeft(arr);
        Console.WriteLine($"{i + 1}. " + string.Join(",", arr));
    }
}

static void RotateLeft(int[] arr)
{
    int temp = arr[0];
    Array.Copy(arr, 1, arr, 0, arr.Length - 1);
    arr[arr.Length - 1] = temp;
}

  
static void Soal4()
{
    Console.WriteLine(" === Soal 4 === ");
    //Masukkan jumlah uang yang dimiliki
    Console.Write("Masukkan Uang Yang Dimiliki =");
    int input = int.Parse(Console.ReadLine());

    //Masukkan harga baju
    Console.Write("Masukkan Harga Baju = ");
    String baju = Console.ReadLine();
    String[] arrayBaju = baju.Split(',');
    int[] Baju = Array.ConvertAll(arrayBaju, int.Parse);

    //Masukkan harga celana
    Console.Write("Masukkan Harga Celana = ");
    String celana = Console.ReadLine();
    String[] arrayCelana = celana.Split(',');
    int[] Celana = Array.ConvertAll(arrayCelana, int.Parse);


    static int TotalBestPrice(int uang, int[] harga_cln, int[] harga_bj)
    {
        int maxtotal = 0;
        foreach (int baju in harga_bj)
        {
            foreach (int celana in harga_cln)
            {
                int totHarga = baju + celana;
                if (totHarga <= uang && totHarga > maxtotal)
                {
                    maxtotal = totHarga;
                }
            }
        }
        return maxtotal;
    }



    static void Soal3()
    {
        Console.WriteLine(" === Soal 3 === ");

        Console.Write("Masukkan kode baju (1-3): ");
        int kodeBaju = int.Parse(Console.ReadLine());

        Console.Write("Masukkan ukuran baju (S/M/L): ");
        string ukuranBaju = Console.ReadLine().ToUpper();

        string[,] dataBaju = {
            { "IMP", "Prada", "Gucci" },   // Merk Baju (Kolom 0)
            { "200000", "150000", "200000" },   // Harga Baju S (Kolom 1)
            { "220000", "160000", "200000" },   // Harga Baju M (Kolom 2)
            { "250000", "170000", "200000" }    // Harga Baju L (Kolom 3)
        };


        if (kodeBaju >= 1 && kodeBaju <= 3)
        {
            int kodeIndex = kodeBaju - 1;
            int ukuranIndex = 0;

            switch (ukuranBaju)
            {
                case "S":
                    ukuranIndex = 1;
                    break;
                case "M":
                    ukuranIndex = 2;
                    break;
                case "L":
                    ukuranIndex = 3;
                    break;
                default:
                    Console.WriteLine("Ukuran baju tidak valid.");
                    return;
            }

            string merkBaju = dataBaju[kodeIndex, 0];
            int hargaBaju = int.Parse(dataBaju[ukuranIndex, kodeIndex]);

            Console.WriteLine("Merk baju: " + merkBaju);
            Console.WriteLine("Harga baju: " + hargaBaju);
        }
        else
        {
            Console.WriteLine("Kode baju tidak valid.");
        }
    }





    static void Soal2()
    {
        Console.WriteLine(" === Soal 2 ===");
        Console.Write(" Masukkan Waktu : ");
        string Input = Console.ReadLine();

        DateTime time = DateTime.ParseExact(Input, "hh:mm:ss tt", null);
        string time24hours = time.ToString("HH:mm:ss");

        Console.WriteLine("Waktu dalam format 24 jam: " + time24hours);

        Console.ReadLine();
    }

    /*static void Soal2()
        {
            Console.WriteLine(" === Soal 2 ===");
            Console.Write(" Masukkan Waktu : ");
            string Input = Console.ReadLine();

            string waktuFormat24Jam = ConvertTo24HourFormat(waktuInput);

            Console.WriteLine("Waktu dalam format 24 jam: " + waktuFormat24Jam);
        }

        static string ConvertTo24HourFormat(string waktuInput)
        {
            string meridiem = waktuInput.Substring(waktuInput.Length - 2);
            string waktu = waktuInput.Substring(0, waktuInput.Length - 2);

            int jam = int.Parse(waktu.Substring(0, 2));
            int menit = int.Parse(waktu.Substring(3, 2));
            int detik = int.Parse(waktu.Substring(6, 2));

            if (meridiem.Equals("PM", StringComparison.OrdinalIgnoreCase) && jam != 12)
            {
                jam += 12;
            }
            else if (meridiem.Equals("AM", StringComparison.OrdinalIgnoreCase) && jam == 12)
            {
                jam = 0;
            }

            string waktuFormat24Jam = $"{jam:D2}:{menit:D2}:{detik:D2}";
            return waktuFormat24Jam;
        }
    */


    static void Soal1()
    {
        Console.WriteLine(" === Soal 1 === ");
        try
        {
        up:
            Console.Write("Masukkan N : ");
            int N = int.Parse(Console.ReadLine());
            Console.Write(" Masukkan nilai awal : ");
            int x = int.Parse(Console.ReadLine());


            for (int i = 1; i <= N; i++)
            {
                int result = i % 2 == 0 ? i * -x : i * x;
                Console.Write($"{result} ");
            }

            Console.WriteLine();
            goto up;
        }
        catch (Exception emessage)
        {
            Console.WriteLine("Masukan Input Hanya Berupa ANGKA AJA YA !!!");
            Console.Write(emessage);
        }
    }
}


/*Console.Write("Masukkan nilai N: ");
int N = int.Parse(Console.ReadLine());

for (int i = 1; i <= N; i++)
{
    int result = i % 2 == 0 ? i * 5 : -i * 5;
    Console.Write($"{result} ");
}

Console.WriteLine();*/


