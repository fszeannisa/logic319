﻿//PerulanganWhile1 () ;
//PerulanganWhile2 () ;
//PerulanganDoWhile();
//PerulanganFor () ;
//PerulanganBreak();
//PerulanganContinue();
//PerulanganForNested();
//PerulanganForEach();
//OperasiLength();
//RemoveString();
//InsertString();
//ReplaceString();
//ContainString();
//SplitandJoin();
//Substring();
//ConvertStringtoCharArray();
//ConvertStringArraytoInt();
//ConvertAll();


Console.ReadKey();

static void ConvertAll()
{
    Console.WriteLine("--ConvertAll--");
    Console.Write("Masukan Input angka (pakai spasi) :");
    string input = Console.ReadLine();

    int sum = 0;

    int[] array = Array.ConvertAll(input.Split(" "), int.Parse);

    foreach (int str in array)
    {
        sum += str;
    }

    Console.WriteLine($"Jumlah = {sum} ");

}
static void ConvertStringArraytoInt()
{
    Console.WriteLine("--ConvertStringArraytoInt--");
    Console.Write("Masukan Input angka (pakai spasi) :");
    string input = Console.ReadLine();

    int sum = 0;

    string[] array = input.Split(" ");

    foreach (string str in array)
    {
        sum = Convert.ToInt32(str);
    }

    Console.WriteLine($"Jumlah = {sum} ");

}
static void ConvertStringtoCharArray()
{
    Console.WriteLine("--ConvertStringtoCharArray--");
    Console.Write("Masukan Kalimat :");
    string kalimat = Console.ReadLine();

    char[] array = kalimat.ToCharArray();

    foreach (char x in array)
    {
        Console.WriteLine(x);
    }

    Console.WriteLine();

    for (int i = 0; i < array.Length; i++)
    {
        Console.WriteLine(array[i]);
    }
}
static void Substring()
{
    Console.WriteLine("--Substring--");
    Console.Write("Masukan kode :");
    string kode = Console.ReadLine();
    Console.Write("Masukan parameter 1 :");
    int param1 = int.Parse(Console.ReadLine());
    Console.Write("Masukan parameter 2 :");
    int param2 = int.Parse(Console.ReadLine());

    if (param2 == 0)
    {
        Console.WriteLine($"Hasil Substring = {kode.Substring(param1)}");
    }
    else
    {
        Console.WriteLine($"Hasil Substring = {kode.Substring(param1, param2)}");
    }


}
static void SplitandJoin()
{
    Console.WriteLine("-- Split and Join--");
    Console.Write("Masukan kalimat :");
    string kalimat = Console.ReadLine();
    Console.Write("Masukan Split :");
    string split = Console.ReadLine();

    string[] katakata = kalimat.Split(split);

    foreach (string kata in katakata)
    {
        Console.WriteLine(kata);
    }
    Console.WriteLine(string.Join(" + ", katakata));

    Console.WriteLine();

    int[] deret = { 1, 2, 3, 4, 5, 6 };
    string strDeret = string.Join("+", deret);
    Console.WriteLine(strDeret);
}
static void ContainString()
{
    Console.WriteLine("Contain String");
    Console.Write("Masukan kata :");
    string kata = Console.ReadLine();
    Console.Write("Masukan contain : ");
    string contain = Console.ReadLine();

    if (kata.Contains(contain))
    {
        Console.WriteLine($" {kata} mengandung {contain}");
    }
    else
    {
        Console.WriteLine($" {kata}  tidak mengandung {contain}");
    }
}
static void ReplaceString()
{
    Console.WriteLine("--Replace String--");
    Console.Write("Masukan kata :");
    string kata = Console.ReadLine();
    Console.WriteLine("Masukan kata yang akan direplace :");
    string kataLama = Console.ReadLine();
    Console.WriteLine("Masukan kata yang baru :");
    string kataBaru = Console.ReadLine();

    Console.WriteLine($"{kata.Replace(kataLama, kataBaru)}");
}
static void InsertString()
{
    Console.WriteLine("Insert String--");
    Console.Write("Masukan kata :");
    string kata = Console.ReadLine();
    Console.Write("Masukan Indeks Insert :");

    int index = int.Parse(Console.ReadLine());
    Console.Write("Masukan Kata Yang di Inser :");
    string insertkata = Console.ReadLine();

    Console.WriteLine($"{kata.Insert(index, insertkata)}");
}
static void RemoveString()
{
    Console.WriteLine("Remove String--");
    Console.Write("Masukan kata :");
    string kata = Console.ReadLine();

    Console.Write("Masukan Indeks Remove :");

    int index = int.Parse(Console.ReadLine());

    Console.WriteLine($"{kata.Remove(index)}");
}
static void OperasiLength()
{
    Console.WriteLine("--Length--");
    Console.Write("Masukan Kata :");
    string kata = Console.ReadLine();

    Console.WriteLine($" Kata {kata} mempunyai panjang karakter = {kata.Length}");
}
static void PerulanganForEach()
{
    int[] array = { 90, 91, 92, 93, 94 };
    int sum = 0;
    foreach (int x in array)
    {
        sum += x;
        Console.WriteLine(x);
    }

    Console.WriteLine($"Jumlah = {sum} ");

    Console.WriteLine();

    for (int i = 0; i < array.Length; i++)
    {
        sum += array[i];
        Console.WriteLine(array[i]);
    }
    Console.WriteLine($"Jumlah = {sum} ");
}
static void PerulanganForNested()
{
    for (int i = 0; i < 3; i++) //iterasibaris
    {
        for (int j = 0; j < 3; j++) //iterasikolom
        {
            Console.Write($"({i},{j})");
        }
        Console.Write("\n");
    }
}
static void PerulanganContinue()
{
    for (int i = 0; i < 10; i++)
    {
        if (i >= 2 && i <= 7)
        {
            continue;
        }
        Console.WriteLine(i);
    }
}
static void PerulanganBreak()
{
    for (int i = 0; i < 10; i++)
    {
        if (i == 6)
        {
            break;
        }
        Console.WriteLine(i);
    }
}
static void PerulanganFor()
{
    for (int i = 0; i < 5; i++)
    {
        Console.WriteLine(i + "\t\n");
    }
    Console.WriteLine("\n");
    for (int i = 9; i >= 0; i--)
    {
        Console.WriteLine(i);
    }
}
static void PerulanganDoWhile()
{
    Console.WriteLine("Perulangan DoWhile");
    Console.Write("Masukan Nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    do
    {
        Console.WriteLine(nilai);
        nilai++;
    }
    while (nilai < 6);

}

static void PerulanganWhile2()
{
    bool ulangi = true;
    Console.WriteLine("Perulangan While 2");
    Console.Write("Masukan Nilai : ");
    int nilai = int.Parse(Console.ReadLine());

    while (ulangi)
    {
        Console.WriteLine($"Proses ke {nilai}");
        nilai++;

        Console.Write("Ulangi proses ? (y/n) : ");
        string input = Console.ReadLine();

        if (input.ToLower() == "n")
        {
            ulangi = false;
        }
    }


}
static void PerulanganWhile1()
{
    Console.WriteLine("Perulangan While ");
    Console.Write("Masukan Nilai :");
    int nilai = int.Parse(Console.ReadLine());

    while (nilai < 6)
    {
        Console.WriteLine(nilai);
        nilai++;
    }
}