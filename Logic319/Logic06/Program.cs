﻿using Logic06;
//AbstractClass();
//Mobil();
//Constructor();
//Encapsulations();
//Inheritance();
Overriding();
Console.ReadKey();

static void Overriding()
{
    Console.WriteLine("=== Overriding ===");
    Kucing kucing = new Kucing();
    Paus paus = new Paus();

    Console.WriteLine ($"Kucing {kucing.pindah()}");
    Console.WriteLine ($"Kucing {paus.pindah()}");
  
}

static void Inheritance()
{
    Console.WriteLine("=== Inheritance ===");
    TypeMobil typeMobil = new TypeMobil();
    typeMobil.Civic();
    
}
static void Encapsulations()
{
    Console.WriteLine(" === Enscapsulation ===");
    PersegiPanjang pp =  new PersegiPanjang();
    pp.panjang = 4.5;
    pp.lebar = 3.5;
    pp.TampilkanData();
}


static void Constructor()
{
    Console.WriteLine("=== Constructor ===");
    Mobil mobil = new Mobil(" B 123 FZ");

    /*string platno = mobil.platno;*/
    string platno = mobil.getPlatno();
    Console.WriteLine($" Mobil dengan Nomor Polisi : {platno}");
}

static void Mobil()
{
    Console.WriteLine("=== Class Mobil===");
    Mobil mobil = new Mobil() { nama = "Ferarri", kecepatan = 0, bensin = 10, posisi =0};
    mobil.platno = "B 234 LX";

    mobil.percepat();
    mobil.maju();
    mobil.isiBensin(20);

    mobil.utama();
}

static void AbstractClass()
{
    Console.WriteLine("=== Abstarct Class ===");
    Console.Write(" Masukan input x : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write(" Masukan input y : ");
    int y = int.Parse(Console.ReadLine());

    TestTurunan calc = new TestTurunan();
    int jumlah = calc.jumlah(x,y);
    int kurang = calc.kurang(x,y);

    Console.WriteLine($" Hasil {x} + {y} = {jumlah}");
    Console.WriteLine($" Hasil {x} - {y} = {kurang}");
}