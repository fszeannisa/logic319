
--SQLDay01

--DDL (Data Definition Language)

-- CREATE
--create database
create database db_kampus

--create table
create table mahasiswa(
id bigint primary key identity(1,1),
name varchar(50) not null,
address varchar(50) not null,
email varchar(255)null
)

--create view
create view vw_mahasiswa 
as select * from mahasiswa

--ALTER
-- add column
alter table mahasiswa add nomor_hp varchar(100) not null

--drop column 
alter table mahasiswa drop column nomor_hp

--alter column
alter table mahasiswa alter column email varchar(100) not null

--DROP
--drop databse
drop database [nama_database]

--drop table 
drop table [nama_table]

--drop view
drop view [nama_view]


--DML (Data Manipulation Language)
--insert
insert into mahasiswa(name, address,email) 
values 
('Fika', 'Palembang','fszeannisa@gmail.com')
('Shofi', 'Muara Enim','shofi@gmail.com'),
('Fika', 'Prabumullih','fika@gmail.com'),
('Zeannisa', 'Palembang', 'zeannisa@gmail.com'),
('Bunga', 'Lampung','Bunga@gmail.com')

--select
select id,name,address,email from mahasiswa

--update
update mahasiswa set name = 'Zeannisa' where id = 1

--delete
delete from mahasiswa where id = 1

--create table biodata
create table biodata (
id bigint primary key identity(1,1),
mahasiswa_id bigint ,
tgl_lahir date not null,
gender varchar(10)not null
)

--insert data table biodata
insert into biodata (mahasiswa_id,tgl_lahir,gender) 
values 
--(1,'1999-01-01','Female'),
--(2,'1999-01-01','male')
(3,'1998-10-01','Female')

--menampilkan data table biodata
select id,mahasiswa_id,tgl_lahir,gender from biodata

--join (ada AND,OR,NOT)
select mhs.id as id_mahasiswa,mhs.name as nama_mahasiswa, mhs.address as alamat, mhs.email as email, bio.tgl_lahir as tanggal_lahir, bio.gender as gender 
from mahasiswa as mhs
join biodata as bio on mhs.id = bio.mahasiswa_id
--where mhs.id = 1
where  mhs.id = 2 and mhs.name = 'zeannisa' or not mhs.name = 'shofi'

--Order by (ASC/DESC)
select * from biodata order by gender desc

--top
select top 1 mahasiswa_id from biodata order by mahasiswa_id asc

--between
select * from biodata where mahasiswa_id between 2 and 3
select * from biodata where tgl_lahir between '1999-01-01' and '2021-10-01'

--like
select * from mahasiswa
where
--name like 'a%'
--name like '%a'
--name like '%ik%' --bebas
--name like '_a%'
--name like  'f___%'
--name like 'z%a'

--group by
select name from mahasiswa group by name

--having 
select name, sum (id) as jumlah 
from mahasiswa group by name 
having sum(id) > 5
order by jumlah

