--Database Penerbit

create database DBPenerbit

--Create tblPengarang
create table tblPengarang(
id int primary key identity (1,1),
Kd_Pengarang varchar(7) not null,
Nama varchar(30) not null,
Alamat varchar(80) not null,
Kota varchar(15) not null,
Kelamin varchar(1) not null
)

--Memasukkan data ke tabel pengarang
insert into  tblPengarang (Kd_Pengarang, Nama, Alamat, Kota, Kelamin)
values 
('P0001','Ashadi', 'Jl.Beo 25', 'Yogya', 'P'),
('P0002','Rian', 'Jl.Solo 123', 'Yogya', 'P'),
('P0003','Suwadi', 'Jl.Semangka 13', 'Bandung', 'P'),
('P0004','Siti', 'Jl.Durian 15', 'Solo', 'W'),
('P0005','Amir', 'Jl.Gajah 33', 'Kudus', 'P'),
('P0006','Suparman', 'Jl.Harimau 25', 'Jakarta', 'P'),
('P0007','Jaja', 'Jl.Singa 7', 'Bandung', 'P'),
('P0008','Saman', 'Jl.Naga 12', 'Yogya', 'P'),
('P0009','Anwar', 'Jl.Tidar 6A', 'Magelang', 'P'),
('P0010','Fatmawati', 'Jl.Renjana 4', 'Bogor', 'W')

--Menampilkan table pengarang
select * from tblPengarang

--create table gaji
create table tblGaji(
id int primary key identity(1,1),
Kd_Pengarang varchar(7) not null,
Nama varchar(30) not null,
Gaji decimal(18,4) not null
)

--Memasukkan data ke table gaji
insert into tblGaji(Kd_Pengarang, Nama, Gaji)
values
('P0002', 'Rian', 600000),
('P0005', 'Amir', 700000),
('P0004', 'Siti', 500000),
('P0003', 'Suwadi', 1000000),
('P0010', 'Fatmawati', 600000),
('P0008', 'Saman', 750000)


--menampilkan table gaji
select * from tblGaji

--Soal 1. Hitung dan tampilkan jumlah pengarang dari table tblPengarang.
select count(Kd_Pengarang) as Jumlah_Pengarang from tblPengarang

--Soal 2. Hitunglah berapa jumlah Pengarang Wanita dan Pria
select Kelamin,
count (Kelamin) as jumlah_kelamin
from tblPengarang
group by Kelamin
having count (Kelamin) > 1

--Soal 3. Tampilkan record kota dan jumlah kotanya dari table
select Kota,
count (Kota) as Jumlah_Kota
from tblPengarang
group by Kota
order by Jumlah_Kota

--Soal 4. Tampilkan record kota diatas 1 kota dari table tblPengarang.
select Kota, count (Kota) as 'Jumlah_Kota'
from tblPengarang
group by Kota
having count (Kota) >1 

--Soal 5. Tampilkan Kd_Pengarang yang terbesar dan terkecil dari table tblPengarang.
select Kd_Pengarang 
from tblPengarang 
where Kd_Pengarang = 'P0001' OR Kd_Pengarang = 'P0010'

--Soal 6. Tampilkan gaji tertinggi dan terendah.
select MIN(Gaji) 
as Gaji_Terendah,
MAX(Gaji) as Gaji_Tertinggi from tblGaji

--Soal 7. Tampilkan gaji diatas 600.000.
select Gaji from tblGaji 
where Gaji > 600000

--Soal 8. Tampilkan jumlah gaji.
select SUM(Gaji) from tblGaji

--Soal 9. Tampilkan jumlah gaji berdasarkan Kota
--select p.kota,sum(g.gaji)as Gaji from tblPengarang as p 
--join tblGaji as g  
--on p.kd_pengarang = g.Kd_pengarang 
--group by p.kota 

select count (*) as count, pen.Kota AS Kota_Pengarang, gaj.Gaji AS Gaji_Pengarang
FROM tblPengarang AS pen
JOIN tblGaji AS gaj ON pen.Kd_Pengarang = gaj.Kd_Pengarang
GROUP BY pen.Kota, gaj.Gaji

--Soal 10. Tampilkan seluruh record pengarang antara P0003-P0006 dari tabel pengarang.
select * from tblPengarang where Kd_Pengarang between 'P0003' and 'P0006'

--Soal 11. Tampilkan seluruh data yogya, solo, dan magelang dari tabel pengarang.
select * from tblPengarang where kota = 'Yogya' or kota = 'Solo' or kota = 'Magelang'

--Soal 12. Tampilkan seluruh data yang bukan yogya dari tabel pengarang.
select * from tblPengarang where not kota  'Yogya' 

--Soal 13. Tampilkan seluruh data pengarang yang nama (terpisah):
select * from tblPengarang where nama like 'A%' --a
select * from tblPengarang where nama like '%i' --b
select * from tblPengarang where nama like '__a%' --c
select * from tblPengarang where nama not like '%n' --d

--Soal 14. Tampilkan seluruh data table tblPengarang dan tblGaji dengan Kd_Pengarang yang sama
select * from tblPengarang as pen 
join tblGaji as gaj
on pen.Kd_Pengarang = gaj.Kd_Pengarang 

--Soal 15. Tampilan kota yang memiliki gaji dibawah 1.000.000	
select * from tblPengarang as pen
join tblGaji as gaj
on pen.Kd_Pengarang = gaj.Kd_Pengarang
where gaji < 1000000 

--Soal 16. Ubah panjang dari tipe kelamin menjadi 10
alter table tblPengarang alter column kelamin varchar(10) 

--17. Tambahkan kolom [Gelar] dengan tipe Varchar (12) pada tabel tblPengarang
alter table tblPengarang add gelar varchar(12) 

--Soal 18. Ubah alamat dan kota dari Rian di table tblPengarang menjadi, Jl. Cendrawasih 65 dan Pekanbaru
update tblPengarang set alamat = 'Jl.Cendrawasih 65', kota = 'Pekanbaru' where nama = 'Rian' 

--Soal 19. Buatlah view untuk attribute Kd_Pengarang, Nama, Kota, Gaji dengan nama vwPengarang
select pen.Kd_Pengarang, pen.Nama, pen.Kota, gaj.Gaji
from tblPengarang as pen
join tblGaji as gaj
on pen.Kd_Pengarang = gaj.Kd_Pengarang