﻿//Soal1();
//Soal2();
//Soal3();
//Soal4();
//Soal5();
//Soal6();
//Soal7();
//Soal8();


Console.ReadKey();




//static void Soal8()
{
    Console.WriteLine("---Soal 8---");
    Console.Write(" Masukan Nilai : ");
    int N = Convert.ToInt32(Console.ReadLine());

    //Menggunakan perulangan for bersarang
        for (int i = 1; i <= N; i++)
        {
            Console.Write(i + "   ");
        }
    Console.WriteLine();

    for (int i = 2; i <= N - 1; i++)
    {
        Console.Write(" *   ");
        for (int j = 2; j <= N - 1; j++)
        {
            Console.Write("    ");
        }
        Console.Write("*");
        Console.WriteLine();
    }

    for (int i = N; i >= 1; i--)
    {
        Console.Write(i + "   ");
    }
    Console.WriteLine();
}

static void Soal7()
    {
        Console.WriteLine("---Soal 7---");
        Console.Write(" Masukan Nilai : ");
        int length = Convert.ToInt32(Console.ReadLine());

        int a = 1;
        int b = 1;

        Console.Write("Output: ");
        Console.Write(a + "," + b);

        for (int i = 2; i < length; i++)
        {
            int c = a + b;
            Console.Write("," + c);

            a = b;
            b = c;
        }

        Console.WriteLine();
    }

//static void Soal6()
//{
//    Console.WriteLine("---Soal 6---");
//    Console.Write(" Masukan N : ");
//    int N = int.Parse(Console.ReadLine());

//    string output = "";

//    int angka = 3;

//    for (int i = 0; i < N; i++)
//    {
//        output += angka.ToString();

//        if (i != N - 1)
//        {
//            output += " * ";
//        }

//        angka *= 3;
//    }

//    Console.WriteLine("Output: " + output);
//}



static void Soal6()
{
    Console.WriteLine("---Soal 6---");
    Console.Write(" Masukan N : ");
    int N = int.Parse(Console.ReadLine());
    Console.Write(" Input bilangan perkalian : ");
    int kali = int.Parse(Console.ReadLine());
    int x = 3;

    for (int i = 0; i < N; i++)
    {
        if (i % 2 == 1)
        {
            Console.Write("* ");
        }
        else
        {
            Console.Write($"{x} ");
            x *= 9;
        }
    }

    Console.WriteLine();
}


static void Soal5()
    {
        Console.WriteLine("---Soal 5---");
        Console.Write(" Masukan Kalimat : ");
    string kalimat = Console.ReadLine();
    string[] kata = kalimat.Split(' ');
    string hasil = "";

    foreach (string kataBaru in kata)
    {
        if (kataBaru.Length > 1)
        {
            hasil += kataBaru.Substring(1) + " ";
        }
    }

    Console.WriteLine("Output: " + hasil.Trim());
}
    



static void Soal4()
{
    Console.WriteLine("---Soal 4---");
    Console.Write(" Masukan Kalimat : ");
    string kalimat = Console.ReadLine();

    string hasil = GantiKata(kalimat);

    Console.WriteLine("hasil : " + hasil);

}
static string GantiKata(string kalimat)
{
    string[] kata = kalimat.Split(' ');
    string hasil = "";

    for (int i = 0; i < kata.Length; i++)
    {
        string kataBaru = "";

        for (int j = 0; j < kata[i].Length; j++)
        {
            if (j == 0 || j == kata[i].Length - 1)
            {
                kataBaru += "*";
            }
            else
            {
                kataBaru += kata[i][j];
            }
        }
        hasil += kataBaru + " ";
    }
    return hasil.Trim();
}

static void Soal3()
{
    Console.WriteLine("---Soal 3---");
    Console.Write(" Masukan Kalimat : ");
    string kalimat = Console.ReadLine();

    string hasil = GantiKarakter(kalimat);

    Console.WriteLine("hasil : " + hasil);

}
static string GantiKarakter(string kalimat)
{
    string[] kata = kalimat.Split(' ');
    string hasil = "";

    for (int i = 0; i < kata.Length; i++)
    {
        string kataBaru = "";

        for (int j = 0; j < kata[i].Length; j++)
        {
            if (j == 0 || j == kata[i].Length - 1)
            {

                kataBaru += kata[i][j];

            }
            else
            {
                kataBaru += "*";
            }
        }
        hasil += kataBaru + " ";
    }
    return hasil.Trim();
}



static void Soal2()
{
    {
        Console.WriteLine("---Soal 2---");
        up:
            Console.Write("Masukkan kalimat: ");
            string kalimat = Console.ReadLine();
        if (kalimat == string.Empty)
        {
            Console.WriteLine(" Pesan Tidak Boleh Kosong");
            goto up;
        }
    
            string[] kata = kalimat.Split(' ');
            Console.WriteLine();

            for (int i = 0; i < kata.Length; i++)
            {
                Console.WriteLine("Kata " + (i + 1) + " = " + kata[i]);
            }

            Console.WriteLine("\nTotal kata adalah " + kata.Length);
        }
    }


    static void Soal1()
    {
        Console.WriteLine("---Soal 1---");
        // Input golongan dan jam kerja
        Console.Write("Golongan: ");
        int golongan = int.Parse(Console.ReadLine());
        Console.Write("Jam Kerja: ");
        int jamKerja = int.Parse(Console.ReadLine());

        // Mendapatkan upah per jam berdasarkan golongan
        int upahPerJam;
        if (golongan == 1)
            upahPerJam = 2000;
        else if (golongan == 2)
            upahPerJam = 3000;
        else if (golongan == 3)
            upahPerJam = 4000;
        else if (golongan == 4)
            upahPerJam = 5000;
        else
        {
            Console.WriteLine("Golongan yang dimasukkan tidak valid.");
            return;
        }

        // Menghitung upah dan lembur
        int upah = jamKerja <= 40 ? jamKerja * upahPerJam : 40 * upahPerJam;
        int lembur = jamKerja > 40 ? (jamKerja - 40) * (upahPerJam * 3 / 2) : 0;
        int total = upah + lembur;

        // Menampilkan hasil
        Console.WriteLine("Upah : \tRp{0:#,0} ", upah);
        Console.WriteLine("Lembur : \tRp{0:#,0} ", lembur);
        Console.WriteLine("Total : \tRp{0:#,0} ", total);


    }

