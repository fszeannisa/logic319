﻿//Konversi();
//OperatorAritmatika();
//Modulus();
//OperatorPenugasan();
//OperatorPerbandingan(); 
//OperatorLogika();
//MethodReturnType();


Console.ReadKey();

static void MethodReturnType()
{
    Console.WriteLine("----METHOD RETURN TYPE");
    Console.Write("Masukkan Mangga : ");
    int mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Apel : ");
    int apel = int.Parse(Console.ReadLine());

    int jumlah = hasil(mangga, apel);

    Console.WriteLine("Hasil Mangga + Apel = {0}", jumlah);
    //Console.WriteLine($"Hasil Mangga + Apel : {jumlah}");
}
static int hasil(int mangga, int apel)
{
    int hasil = 0;

    hasil = mangga + apel;


    return hasil;
}

static void OperatorLogika()
{
    Console.WriteLine("---OPERATOR LOGIKA---");
    Console.Write("Enter Your Age : ");
    int age = int.Parse(Console.ReadLine());
    Console.Write("Password : ");
    string password = (Console.ReadLine());

    bool isAdult = age > 18;
    bool isPasswordValid = password == "admin";

    if (isAdult && isPasswordValid)
    {
        Console.WriteLine("WELCOME HOME!!!");
    }
    else
    {
        Console.WriteLine("SORRY TRY AGAIN");
    }
}


static void OperatorPerbandingan()
{
    int mangga, apel = 0;
    Console.WriteLine("---OPERATOR PERBANDINGAN---");
    Console.Write("Masukkan Mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Apel : ");
    apel = int.Parse(Console.ReadLine());

    Console.WriteLine("Hasil Perbandingan : ");
    Console.WriteLine($"Mangga > apel : {mangga > apel}");
    Console.WriteLine($"Mangga >= apel : {mangga >= apel}");
    Console.WriteLine($"Mangga < apel : {mangga < apel}");
    Console.WriteLine($"Mangga <= apel : {mangga <= apel}");
    Console.WriteLine($"Mangga == apel : {mangga == apel}");
    Console.WriteLine($"Mangga != apel : {mangga != apel}");
}

static void OperatorPenugasan()
{
    int mangga = 10;
    int apel = 8;


    //isi ulang variabel
    //mangga = 15;

    Console.WriteLine("-----OPERATOR PENUGASAN-----");
    Console.Write("Masukkan Mangga : ");
    mangga = int.Parse(Console.ReadLine()); //INPUT
    Console.WriteLine($"mangga : {mangga}");

    //Opreator Penugasan
    //apel += 6;
    Console.Write("Masukkan Apel : ");
    apel = int.Parse(Console.ReadLine()); //INPUT
    Console.WriteLine($"apel : {apel}");

}


static void Modulus()
{
    int mangga, orang, hasil = 0;

    Console.WriteLine("Modulus");
    Console.Write("Masukkan Mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Orang : ");
    orang = int.Parse(Console.ReadLine());

    hasil = mangga % orang;

    Console.WriteLine($"Hasil mangga % orang = {hasil}");

}

static void OperatorAritmatika()
{
    int mangga, apel, hasil = 0;

    Console.WriteLine("--Operator Aritmatika--");
    Console.Write("Masukan mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukan apel : ");
    apel = int.Parse(Console.ReadLine());

    hasil = mangga + apel;

    Console.WriteLine($"Hasil mangga + apel ={hasil}");

}

static void Konversi()
{
    Console.WriteLine("--Konversi--");

    int myInt = 100;
    double myDouble = 5.25;
    bool myBool = true;

    string strMyInt = Convert.ToString(myInt);
    string strMyInt2 = myInt.ToString();

    Console.WriteLine(strMyInt);
    Console.WriteLine(strMyInt2);
    Console.WriteLine(Convert.ToDouble(myInt));
    Console.WriteLine(Convert.ToString(myDouble));
    Console.WriteLine(Convert.ToString(myBool));
};