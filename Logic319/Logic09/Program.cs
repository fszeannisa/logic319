﻿FungsiMath();
Console.ReadKey();

static void aVeryBigSum()
{
    long result = 0;
    result = aVeryBigSum(List);
    Console.WriteLine($" Hasil aVeryBigSum = {result}");
}
static void FungsiMath()
{
    Console.WriteLine("=== Fungsi Math ===");
    Console.Write(" Masukkan input angka : ");
    decimal angka = decimal.Parse(Console.ReadLine());

    Console.WriteLine($" Hasil fungsi Absolute : {Math.Abs(angka)}");
    Console.WriteLine($" Hasil Fungsi Round : {Math.Round(angka)}");
    Console.WriteLine($" Hasil Fungsi Round 2 Digit Desimal: {Math.Round(angka,2)}");
    Console.WriteLine($" Hasil Fungsi Round 2 Digit Desimal (bulat kebawah): {Math.Round(angka,2, MidpointRounding.ToNegativeInfinity)}");
    Console.WriteLine($" Hasil Fungsi Round 2 Digit Desimal (bulat keatas): {Math.Round(angka,2, MidpointRounding.ToPositiveInfinity)}");
    Console.WriteLine($" Hasil Fungsi Pow : {Math.Pow(Convert.ToDouble(angka),2)}");
}