﻿//PadLeft();
//RekursifDESC();
//RekursifASC();

Console.ReadKey();

static void RekursifASC()
{
    Console.WriteLine(" === Rekursif Fungtion ASC ===");
    Console.Write(" Masukkan Input awal : ");
    int start = int.Parse(Console.ReadLine());
    Console.Write(" Masukkan input akhir :");
    int end = int.Parse(Console.ReadLine());

    if (start <= end)
    {
        PerulanganASC(start, end);
    }
    else
    {
        Console.WriteLine("Input awal harus lebih besar dari input akhir");
    }
    //panggil fungsi 
    PerulanganASC(start, end);
}
static int PerulanganASC(int start, int end)
{
    if (end == start)
    {
        Console.WriteLine(start);
        return start;
    }
    Console.WriteLine(start);
    return PerulanganASC(start+1, end);
}
static void RekursifDESC()
{
    Console.WriteLine(" === Rekursif Fungtion ASC ===");
    Console.Write(" Masukkan Input awal : ");
    int start = int.Parse(Console.ReadLine());
    Console.Write(" Masukkan input akhir :");
    int end = int.Parse(Console.ReadLine());

    if (start <= end )
    {
        PerulanganASC(start, end);
    }
    else
    {
        Console.WriteLine("Input awal harus lebih besar dari input akhir");
    }
    //panggil fungsi 
    PerulanganDESC(start, end);
}
static int PerulanganDESC(int start, int end)
{
    if (end == start)
    {
        Console.WriteLine(end);
        return end;
    }
    Console.WriteLine(end);
    return PerulanganDESC(start, end - 1);
}

static void PadLeft()
{
    Console.WriteLine("=== PadLeft ===");
    Console.Write(" Masukkan input  :  ");
    int input = int.Parse(Console.ReadLine());
    Console.Write(" Masukkan Panjang Karakter : ");
    int panjang = int.Parse(Console.ReadLine());
    Console.Write(" Masukkan Karakter :");
    char chars = char.Parse(Console.ReadLine());

    //20230600005 -> 2 digit rahun, 2 digit bulan, generate length
    DateTime date = DateTime.Now;

    string code = "";

    code = date.ToString("yyyy") + date.ToString("MM") + input.ToString().PadLeft(panjang, chars);
   // code = date.ToString("yyyy") + date.ToString("MM") + input.ToString().PadRight(panjang, chars);

    Console.WriteLine($" Hasil PadLeft : {code}");


}
