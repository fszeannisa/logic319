﻿using Logic04;
//InitializeArray();
//AksesElemenArray();
//Array2Dimensi();
//InitializeList();
//PanggilClassStudent();
//AksesElemenList();
//InsertList();
//RemoveList();
//IndeksElemenList();
//InitializeDateTime();
//InitializeDateTime();
//DateTimePharsing();
//DateTimeProperties();
//workingwithtimespan();
Console.ReadKey();

static void workingwithtimespan()
{
    Console.WriteLine("---workingwithtimespan---");
    DateTime date1 = new DateTime(2023,1,10,11,20,30);
    DateTime date2 = new DateTime(2023, 2, 20, 12, 25, 35);

    TimeSpan interval = date1 - date2;

    Console.WriteLine(" No of Days :" + interval.Days);
    Console.WriteLine(" Total No of Days :" +  interval.TotalDays);
    Console.WriteLine("  No of Hours :" + interval.Hours);
    Console.WriteLine(" Total  No of Hours :" + interval.TotalHours);
    Console.WriteLine(" No of Minutes :"+ interval.Minutes);
    Console.WriteLine(" Total No of Minutes :" + interval.TotalMinutes); 
    Console.WriteLine(" No of Seconds :"+ interval.Seconds); 
    Console.WriteLine(" Total No of Seconds:" + interval.TotalSeconds); 
    Console.WriteLine(" No of MilliSeconds:" + interval.Milliseconds); 
    Console.WriteLine(" Total No of MiliiSeconds:" + interval.TotalMilliseconds);
    Console.WriteLine("Total : " + interval.Ticks);

}

static void DateTimeProperties()
{
    Console.WriteLine(" ---DateTimeProperties---");
    DateTime myDate = new DateTime(2023, 6, 2, 14, 49, 50);
    int year = myDate.Year;//2023
    int month = myDate.Month;//06
    int day = myDate.Day;//2
    int hour = myDate.Hour;//14
    int minute = myDate.Minute;//49
    int second = myDate.Second;//50
    int weekday =(int) myDate.DayOfWeek;
    string dayString = myDate.ToString("dddd");
    string dayString2 = myDate.DayOfWeek.ToString();

    Console.WriteLine($"tahun : (tahun)");
    Console.WriteLine($"tahun : (bulan)");
    Console.WriteLine($"tahun : (tanggal)");
    Console.WriteLine($"tahun : (jam)");
    Console.WriteLine($"tahun : (menit)");
    Console.WriteLine($"tahun : (detik)");
    Console.WriteLine($"tahun : (weekday)");
    Console.WriteLine($"tahun : (dayString1)");
    Console.WriteLine($"tahun : (dayString2)");


}

static void DateTimePharsing()
{
    Console.WriteLine("--DateTimePharsing--");
    Console.Write("Masukan Date Time (MM/dd/yyyy) : ");
    string dateString = Console.ReadLine();

    try
    {
        DateTime date= DateTime.ParseExact(dateString, "MM/dd/yyyy", null);
        Console.WriteLine(date);
    }
    catch(Exception e)
    {
    Console.WriteLine(" Format yang anda masukan salah !");
}
  
}
static void InitializeDateTime()
{
    DateTime dt1 =new  DateTime();
    Console.WriteLine(dt1);

    DateTime dtnow = DateTime.Now;
    Console.WriteLine(dtnow);

    DateTime dt2  = new DateTime (2023, 6, 2);
    Console.WriteLine(dt2);

    DateTime dt3 = new DateTime(2021, 6, 2, 13, 27, 45);
    Console.WriteLine(dt3);
}
static void IndeksElemenList()
{
    Console.WriteLine("--IndeksElemenList--");
    List<int> list = new List<int>();
    list.Add(1);
    list.Add(2);
    list.Add(3);

    int item = '3';
    int index = list.IndexOf(item);


    if (index != -1)
    {
        Console.WriteLine($"Elemen is found at index {index}");
    }

    for (int i = 0; i < list.Count; i++)
    {
        Console.WriteLine(list[i]);
    }
}
static void RemoveList()
{
    Console.WriteLine("--RemoveList--");
    List<int> list = new List<int>();
    list.Add(1);
    list.Add(2);
    list.Add(3);

    list.Remove('1');//keremove hanya 1 data
    list.RemoveAt(1);
   
    for (int i = 0; i < list.Count; i++)
    {
        Console.WriteLine(list[i]);
    }
}

static void InsertList()
{
    Console.WriteLine("--InsertList--");
    List<int> list = new List<int>();
    list.Add(1);
    list.Add(2);
    list.Add(3);

    list.Insert(2,4);
    for (int i = 0; i < list.Count; i++)
    {
        Console.WriteLine(list[i]);
    }

}

static void AksesElemenList()
{
    Console.WriteLine("--AksesElemenList--");
    //List<int> list = new List<int> { 1, 2, 3, };
    List<int> list = new List<int>();
    list.Add(1);
    list.Add(2);    
    list.Add(3);    


    Console.WriteLine(list[1]);
    Console.WriteLine(list[2]);
    Console.WriteLine(list[3]);

    foreach (int item in list)
    {
        Console.WriteLine(item);
    }
    for (int i = 0; i < list.Count; i++)
    {
        Console.WriteLine(list);
    }

}
static void PanggilClassStudent()
{
    List<Student>students = new List<Student>()
    {
        new Student() { Id = 1, Name = " John Doe" },
        new Student() { Id = 2, Name = " Jane Doe" },
        new Student() { Id = 3, Name = " Joe Doe" }
    };
    Console.WriteLine($"Panjang data list student = {students.Count}");
    foreach (Student item in students )
    {
        Console.WriteLine($"Id : {item.Id}, Name : {item.Name}");
    }
  
}

static void InitializeList()
{
    Console.WriteLine("--InitializeList()--");
    List<string> list = new List<string>()
    {
        "John Doe",
        "Jane Doe",
        "Joe Doe"
    };
    list.Add("Joko Doe");

    Console.WriteLine(string.Join(",",list));
}

static void Array2Dimensi()
{
    int[,] array = new int [3, 3 ]
    {
        { 2001, 2002, 2003},
        { 2004, 2005, 2006},
        { 2007,2008,2009}
        
    };
    for (int i = 0; i < array.GetLength(1); i++)
    {
        for (int j = 0; j < array.GetLength(2); j++)
        {
            Console.WriteLine(array[i, j]);
        }
        Console.WriteLine();
    }
}
static void AksesElemenArray()
{
    Console.WriteLine("--Akses Elemen Array--");
    int[] intStaticArray = new int[1];
    intStaticArray[1] = 1;
    intStaticArray[1] = 2;
    intStaticArray[1] = 3;

    Console.WriteLine(intStaticArray[1]);
    Console.WriteLine(intStaticArray[2]);
    Console.WriteLine(intStaticArray[3]);

    int[] array = { 2, 4, 5 };
    for (int i = 0; i < array.Length; i++)
    {
        Console.WriteLine(array[1]);
    }
    string[] strArray = new string[]{" Mahesh Chand, Mike Gold, Denish Beniwal"};

    foreach (string item in strArray)
    {
        Console.WriteLine(item);
    }
}

static void InitializeArray()
{
    Console.WriteLine("--Inisialisasi Array--");
    //Cara1
    int[] array = new int[5];
    // array[0] = 1;
    for (int i = 0; i < array.Length; i++)
    {
        Console.Write($" Masukan data ke {i +1}: ");
        array[i] =  int.Parse(Console.ReadLine());
    }
    //Cara2
    int[] array2 = new int[5] {1, 2, 3, 4, 5};
    //Cara3
    int[] array3 = new int[] { 1, 2, 3, 4, 5 };
    //Cara4
    int[] array4 = {1,2, 3,4, 5};
    //Cara5
    int array5;
   // array5= new int[5] {1,2,3,4,5};
    Console.WriteLine(string.Join (",",array));
    Console.WriteLine(string.Join(",",array2));
    Console.WriteLine(string.Join(",",array3));
    Console.WriteLine(string.Join(",",array4));
  //  Console.WriteLine(string.Join(",",array5));
   
}